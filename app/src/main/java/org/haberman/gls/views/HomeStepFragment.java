package org.haberman.gls.views;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v17.leanback.widget.GuidanceStylist;
import android.support.v17.leanback.widget.GuidedAction;

import org.haberman.gls.R;

import java.util.List;
import java.util.Objects;

import static org.haberman.gls.MainActivity.IC_CHECK_RES_IDS;
import static org.haberman.gls.views.NextStepFragment.STEP_NEXT_ID;

public class HomeStepFragment extends StepFragment
{
    public final static int STEP_HOME_ID = 0x001;
    
    @Override
    @NonNull
    public GuidanceStylist.Guidance onCreateGuidance( @NonNull Bundle savedInstanceState ) {
        final String title       = getString( R.string.nav_home_title );
        final String breadcrumb  = getString( R.string.nav_home_breadcrumb );
        final String description = getString( R.string.nav_home_description );
        final Drawable icon = Objects.requireNonNull( getActivity() )
                                     .getDrawable( R.drawable.ic_home );
        return new GuidanceStylist.Guidance( title, description, breadcrumb, icon );
    }
    
    @Override
    public void onCreateActions( @NonNull List<GuidedAction> actions, Bundle bundle ) {
        super.onCreateActions( actions, bundle );
        
        add_checked_action( actions, STEP_HOME_ID, IC_CHECK_RES_IDS,
                            String.format( getString( R.string.nav_checkable_title ), 1 ),
                            getString( R.string.nav_checkable_description ), false );
        add_checked_action( actions, STEP_HOME_ID, IC_CHECK_RES_IDS,
                            String.format( getString( R.string.nav_checkable_title ), 2 ),
                            getString( R.string.nav_checkable_description ), false );
        add_action( actions, STEP_NEXT_ID,
                    getString( R.string.nav_normal_title ),
                    getString( R.string.nav_normal_description ), true, false );
    }
    
    @Override
    public void onGuidedActionClicked( GuidedAction action ) {
        if (action.getId() == STEP_HOME_ID) { switch_checked_action( action ); }
        super.onGuidedActionClicked( action );
    }
}