package org.haberman.gls.views;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v17.leanback.widget.GuidanceStylist;
import android.support.v17.leanback.widget.GuidedAction;

import org.haberman.gls.R;

import java.util.List;
import java.util.Objects;

public class NextStepFragment extends StepFragment
{
    public static final int STEP_NEXT_ID = 0x002;
    
    @Override
    @NonNull
    public GuidanceStylist.Guidance onCreateGuidance( @NonNull Bundle savedInstanceState ) {
        final String title       = getString( R.string.nav_next_title );
        final String breadcrumb  = getString( R.string.nav_next_breadcrumb );
        final String description = getString( R.string.nav_next_description );
        final Drawable icon = Objects.requireNonNull( Objects.requireNonNull( getActivity() ) )
                                     .getDrawable( R.drawable.ic_next );
        return new GuidanceStylist.Guidance( title, description, breadcrumb, icon );
    }
    
    @Override
    public void onCreateActions( @NonNull List<GuidedAction> actions, Bundle bundle ) {
        super.onCreateActions( actions, bundle );
        
        final Bundle args = getArguments();
        add_action( actions,
                    STEP_NEXT_ID,
                    Objects.requireNonNull( args ).getString( "title" ),
                    args.getString( "description" ),
                    true,
                    true );
    }
    
    @Override
    public void onGuidedActionClicked( GuidedAction action ) {
        final boolean checked = action.isChecked();
        action.setChecked( !checked );
        switch_checked_action( action );
        
        super.onGuidedActionClicked( action );
    }
}
