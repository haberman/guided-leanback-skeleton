package org.haberman.gls.views;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.v17.leanback.app.GuidedStepSupportFragment;
import android.support.v17.leanback.widget.GuidedAction;

import org.haberman.gls.R;

import java.util.List;

public class StepFragment extends GuidedStepSupportFragment
{
    
    @Override
    @StyleRes
    public int onProvideTheme() {
        return R.style.TVicoTheme_Leanback_GuidedStep;
    }
    
    @Nullable
    StepActionListener action_listener;
    
    @DrawableRes
    int[] ic_status_res = new int[ 2 ];
    
    @Override
    public void onCreateActions( @NonNull List<GuidedAction> actions, Bundle bundle ) {
        if (action_listener != null) { action_listener.on_step_created( this ); }
    }
    
    @Override
    public void onGuidedActionClicked( GuidedAction action ) {
        if (action_listener != null) { action_listener.on_action_clicked( action ); }
    }
    
    public void add_action( List<GuidedAction> actions,
                            final long id,
                            final String title,
                            final String desc,
                            final boolean multiline,
                            final boolean infoOnly ) {
        
        actions.add( new GuidedAction.Builder( getContext() )
                             .id( id )
                             .title( title )
                             .description( desc )
                             .multilineDescription( multiline )
                             .infoOnly( infoOnly )
                             .build() );
    }
    
    public void switch_checked_action( final GuidedAction replacement ) {
        List<GuidedAction> actions = getActions();
        
        final int at = actions.indexOf( replacement );
        if (at == -1) { return; }
        
        actions.remove( at );
        actions.add( at, new GuidedAction.Builder( getContext() )
                .id( replacement.getId() )
                .title( replacement.getTitle() )
                .description( replacement.getDescription() )
                .checked( !replacement.isChecked() )
                .icon( replacement.isChecked() ? ic_status_res[ 0 ] : ic_status_res[ 1 ] )
                .build() );
        
        setActions( actions );
    }
    
    public void add_checked_action( List<GuidedAction> actions,
                                    final int id,
                                    @DrawableRes int[] iconRes,
                                    final String title,
                                    final String desc,
                                    final boolean checked ) {
        ic_status_res = iconRes;
        actions.add( new GuidedAction.Builder( getContext() )
                             .id( id )
                             .title( title )
                             .description( desc )
                             .checked( checked )
                             .icon( checked ? ic_status_res[ 1 ] : ic_status_res[ 0 ] )
                             .build() );
    }
    
    public void set_action_listener( StepActionListener listener ) { action_listener = listener; }
    
    public interface StepActionListener
    {
        void on_step_created( StepFragment fragment );
        
        void on_action_clicked( final GuidedAction action );
    }
}
