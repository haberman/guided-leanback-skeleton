/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package org.haberman.gls;

import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.v17.leanback.app.GuidedStepSupportFragment;
import android.support.v17.leanback.widget.GuidedAction;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import org.haberman.gls.views.HomeStepFragment;
import org.haberman.gls.views.NextStepFragment;
import org.haberman.gls.views.StepFragment;

import static org.haberman.gls.views.NextStepFragment.STEP_NEXT_ID;

/*
 * MainActivity class that loads {@link MainFragment}.
 */
public class MainActivity extends FragmentActivity implements StepFragment.StepActionListener
{
    @DrawableRes
    public static final int[] IC_CHECK_RES_IDS = { R.drawable.ic_plug_off, R.drawable.ic_plug_on };
    
    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        
        final StepFragment home_fragment = new HomeStepFragment();
        home_fragment.set_action_listener( this );
        
        GuidedStepSupportFragment.addAsRoot( this, home_fragment, android.R.id.content );
    }
    
    @Override
    public void on_step_created( StepFragment fragment ) {
        Log.i( "Main", "on_step_created: by class: " + fragment.getClass().getSimpleName() );
        
    }
    
    @Override
    public void on_action_clicked( GuidedAction action ) {
        if (action.getId() == STEP_NEXT_ID) {
            final Bundle args = new Bundle();
            args.putString( "title", "Next details" );
            args.putString( "description",
                            "This can contain dynamic content coming from elsewhere" );
            
            final NextStepFragment next_frag = new NextStepFragment();
            next_frag.setArguments( args );
            
            GuidedStepSupportFragment.add( getSupportFragmentManager(), next_frag );
        }
    }
}
